<?php

require __DIR__ . '/../../vendor/autoload.php';

use Pixelbrackets\HumansInSpace\HumansInSpace;

header('Content-type:application/json;charset=utf-8');

$responseBody = [
    'number' => (new HumansInSpace())->getNumber(),
];

echo json_encode($responseBody);

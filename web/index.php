<?php

require __DIR__ . '/../vendor/autoload.php';

$template = new \Pixelbrackets\Html5MiniTemplate\Html5MiniTemplate();
$template->setTitle('🚀 People in Space Webapp');
$template->setStylesheet('/assets/skeleton.css');
$template->setAdditionalMetadata('<script src="/assets/vue.js"></script><link rel="manifest" href="/manifest.webmanifest"><meta name="theme-color" content="#222" />');
$template->setContent('
  <div id="app" class="container">
    <h1>🚀 {{ peopleInSpace.number }} {{ message }}</h1>
  </div>

  <script>
    var app = new Vue({
      el: "#app",
      data: {
        message: "humans are in space right now",
        peopleInSpace: []
      },
      created(){
        fetch("/api/")
        .then(response => response.json())
        .then(json => {
          console.log("JSON", json)
          this.peopleInSpace = json
        })
        .catch(error => {
          console.log(error);
        });
      }
    })
  </script>
');
echo $template->getMarkup();

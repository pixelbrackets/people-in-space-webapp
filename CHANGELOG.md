# Changelog

2020-09-25 Dan Untenzu <mail@pixelbrackets.de>

  * 1.3.0
  * FEATURE Allow all orientations
  * FEATURE PWA: Add shortname
  * FEATURE Use API wrapper package
  * FEATURE Docs: Change demo URL

2020-04-23 Dan Untenzu <mail@pixelbrackets.de>

  * 1.2.0
  * FEATURE Add vue.js to frontend
  * FEATURE Add webmanifest

2020-04-23 Dan Untenzu <mail@pixelbrackets.de>

  * 1.1.0
  * FEATURE Add CI

2020-04-23 Dan Untenzu <mail@pixelbrackets.de>

  * 1.0.0
  * FEATURE Inital version
